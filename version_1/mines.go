package version_1

import (
	"fmt"
	"github.com/oelmekki/matrix"
	"strconv"
)

func printMatrix(matrix *matrix.Matrix) {

	for i := 0; i < matrix.Rows(); i++ {
		for j := 0; j < matrix.Cols(); j++ {
			fmt.Print(strconv.Itoa(int(matrix.At(i, j))) + " ")
		}
		fmt.Println()
	}

}

func solveBoard(matrix *matrix.Matrix, x int, y int) bool {

	if (x == 498) && (y == 498) {
		return true
	}

	if (x >= 0) && (y >= 0) && (x < 499) && (y < 499) && (matrix.At(x, y) == 0) {

		matrix.SetAt(x, y, -1)

		//going all directions
		if solveBoard(matrix, x+1, y) {
			return true
		}

		if solveBoard(matrix, x, y+1) {
			return true
		}

		if solveBoard(matrix, x-1, y) {
			return true
		}

		if solveBoard(matrix, x, y-1) {
			return true
		}

		matrix.SetAt(x, y, 2)
		return false
	}
	return false

}

var myMatrix = matrix.GenerateMatrix(499, 499)

func main() {

	for i := 0; i < myMatrix.Rows(); i++ {
		for j := 0; j < myMatrix.Cols(); j++ {
			if IsMine(i, j) {
				myMatrix.SetAt(i, j, 1)
			}

		}
	}

	solveBoard(&myMatrix, 0, 0)

	printMatrix(&myMatrix)

	//count how many 2s we have
	steps := 0
	for i := 0; i < myMatrix.Rows(); i++ {
		for j := 0; j < myMatrix.Cols(); j++ {
			if myMatrix.At(i, j) == 2 {
				steps += 1
			}
		}
	}

	fmt.Printf("\n Total steps: %d\n", steps)

}
