package main

import (
	"fmt"
)

var visitedStates = NewSet()
var statesToVisist = &Queue{nodes: make([]*Node, 100000000)}

func checkNeighbours(point Point) []Point {
	var neighbours []Point

	if point.x < 0 || point.y < 0 {
		return nil
	}

	neighbours = append(neighbours, Point{point.x, point.y + 1})
	neighbours = append(neighbours, Point{point.x, point.y - 1})
	neighbours = append(neighbours, Point{point.x - 1, point.y})
	neighbours = append(neighbours, Point{point.x + 1, point.y})

	return neighbours
}

func main() {

	counter := 0

	statesToVisist.Push(&Node{Point{0, 0}})
	visitedStates.Add(Point{0, 0})

	//while we have places to visit
	for statesToVisist.count != 0 {

		mNode := statesToVisist.Pop()

		neightbours := checkNeighbours(Point{mNode.Value.x, mNode.Value.y})

		for _, neightbourPoint := range neightbours {

			if !IsMine(neightbourPoint.x, neightbourPoint.y) {

				if !visitedStates.Contains(neightbourPoint) {
					statesToVisist.Push(&Node{neightbourPoint})
					visitedStates.Add(neightbourPoint)
					counter++

				}
			}

		}

	}

	fmt.Printf("TOTAL Steps = %d", counter)

}
