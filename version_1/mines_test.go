package version_1

import (
	"testing"
	"github.com/oelmekki/matrix"
)

func BenchmarkWithoutGorutines(b *testing.B) {
	var myMatrix = matrix.GenerateMatrix(499, 499)
	for n := 0; n < b.N; n++ {
		solveBoard(&myMatrix, 0, 0)
	}
}
