# maxtrix play

It seems to be a type of rat in a maze // backtracking problem.

I started out in the simplest way possible, doing a visualization of the "map".

"github.com/oelmekki/matrix" is a library that has some matrix related functions. no need to reinvent the wheel

let's say 300 rows/cols should be enough to get an idea what is going on.

~~~~
package main

import (
	"fmt"
	"github.com/oelmekki/matrix"
	"strconv"
)


func printMatrix(matrix matrix.Matrix){

	for i := 0; i < matrix.Rows(); i++ {
		for j := 0; j < matrix.Cols(); j++ {
			fmt.Print( strconv.Itoa(int(matrix.At(i,j))) + " ")
		}
		fmt.Println()
	}

}


func main() {

	myMatrix := matrix.GenerateMatrix(300, 300)

	for i := 0; i < myMatrix.Rows(); i++ {
		for j := 0; j < myMatrix.Cols(); j++ {
			if IsMine(i,j) {
				myMatrix.SetAt(i, j, 1)
			}

		}
	}

	printMatrix(myMatrix)

}
~~~~

I need an IsMine function, so I need to calculate the digits of a number.

Let's write some tests

~~~~
func TestSumOfDigitsSimple(t *testing.T) {
	total := getSumOfDigits(112)
	if total != 4 {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", total, 4)
	}
}


func TestSumOfDigitsNegative(t *testing.T) {
	total := getSumOfDigits(-11111)
	if total != 5 {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", total, 5)
	}
}


// edge cases
func TestSumOfDigitEdge(t *testing.T) {
	total := getSumOfDigits(math.MaxInt64)
	if total != 88 {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", total, 88)
	}
}
~~~~

so this function SumOfDIgits is:

~~~~

// gets sum of the digits of the number (absolute value)
func getSumOfDigits(nr int) int {
	strs := strings.Split(strconv.Itoa(nr), "")

	// negative numbers are cut
	if strs[0] == "-" {
		strs = strs[1:]
	}

	// store them in an array
	ary := make([]int, len(strs))
	for i := range ary {
		ary[i], _ = strconv.Atoi(strs[i])
		ary[i] = int(ary[i])
	}

	// calculate the sum
	sum := 0
	for _, num := range ary {
		sum += num
	}
	return sum
}
~~~~


the isMine function is simple

~~~~
// return true if it's a mine, false if it's not
func IsMine(x int, y int) bool {

	sumX := getSumOfDigits(x)
	sumY := getSumOfDigits(y)

	if sumX+sumY > 21 {
		return true
	}
	return false
}
~~~~

with some tests just to be sure 100% correct


~~~~
func TestMineNegative(t *testing.T) {
	isMine := IsMine(1,1)
	if isMine {
		t.Errorf("Sould not be isMine = true here.")
	}
}

func TestMineNegative2(t *testing.T) {
	isMine := IsMine(-113,-104)
	if isMine {
		t.Errorf("Sould not be isMine = true here.")
	}
}

func TestMinePositive(t *testing.T) {
	isMine := IsMine(59,-79)
	if !isMine {
		t.Errorf("Sould not be isMine = false here.")
	}
}

func TestMinePositive2(t *testing.T) {
	isMine := IsMine(99,99)
	if !isMine {
		t.Errorf("Sould not be isMine = false here.")
	}
}
~~~~


final tools file looks like this

~~~~
package main

import (
	"strings"
	"strconv"
)

// gets sum of the digits of the number (absolute value)
func getSumOfDigits(nr int) int {
	strs := strings.Split(strconv.Itoa(nr), "")

	// negative numbers are cut
	if strs[0] == "-" {
		strs = strs[1:]
	}

	// store them in an array
	ary := make([]int, len(strs))
	for i := range ary {
		ary[i], _ = strconv.Atoi(strs[i])
		ary[i] = int(ary[i])
	}

	// calculate the sum
	sum := 0
	for _, num := range ary {
		sum += num
	}
	return sum
}

// return true if it's a mine, false if it's not
func IsMine(x int, y int) bool {

	sumX := getSumOfDigits(x)
	sumY := getSumOfDigits(y)

	if sumX+sumY > 21 {
		return true
	}
	return false
}
~~~~


when the graph is displayed it looks like this

<a href="minemap.png"><img src="minemap.png"></a>

now, I see what is the maximum Y for the first row. so the point (0,Y) that is a mine ( > 21)

~~~~
	for y := 0; y < 1000; y++ {
		if IsMine(0,y) {
			fmt.Printf("i = 0, y = %d", y)
		}
	}
~~~~

I get: i = 0, y = 499

so the max cols of the matrix are 499. This would be true for the rows. So we have a matrix of 499x499.

backtracking goes ok with this.


~~~~
package main

import (
	"fmt"
	"github.com/oelmekki/matrix"
	"strconv"
	"sync"
)

func printMatrix(matrix *matrix.Matrix) {

	for i := 0; i < matrix.Rows(); i++ {
		for j := 0; j < matrix.Cols(); j++ {
			fmt.Print(strconv.Itoa(int(matrix.At(i, j))) + " ")
		}
		fmt.Println()
	}

}

func solveBoard(matrix *matrix.Matrix, x int, y int) bool {

	if (x == 498) && (y == 498) {
		return true
	}

	if (x >= 0) && (y >= 0) && (x < 499) && (y < 499) && (matrix.At(x, y) == 0) {

		wg.Add(4)

		//going all directions
		go func() {
			defer wg.Done()
			solveBoard(matrix, x+1, y)
		}()

		go func() {
			defer wg.Done()
			solveBoard(matrix, x, y+1)
		}()

		go func() {
			defer wg.Done()
			solveBoard(matrix, x-1, y)
		}()

		go func() {
			defer wg.Done()
			solveBoard(matrix, x, y-1)
		}()

		matrix.SetAt(x, y, 2)
		return false
	}
	return false

}

var myMatrix = matrix.GenerateMatrix(499, 499)
var wg sync.WaitGroup

func main() {

	for i := 0; i < myMatrix.Rows(); i++ {
		for j := 0; j < myMatrix.Cols(); j++ {
			if IsMine(i, j) {
				myMatrix.SetAt(i, j, 1)
			}

		}
	}

	solveBoard(&myMatrix, 0, 0)
	wg.Wait()

	printMatrix(&myMatrix)

	//count how many 2s we have
	steps := 0
	for i := 0; i < myMatrix.Rows(); i++ {
		for j := 0; j < myMatrix.Cols(); j++ {
			if myMatrix.At(i, j) == 2 {
				steps += 1
			}
		}
	}

	fmt.Printf("\n Total steps: %d\n", steps)

}
~~~~~

Result: Total steps: 72469

Check image:

<a href="minemap2.png"><img src="minemap2.png"></a>

### Do gorutines actually help ? Let's benchmark it

~~~~
func BenchmarkWithGorutines(b *testing.B) {
	var myMatrix = matrix.GenerateMatrix(499, 499)
	for n := 0; n < b.N; n++ {
		solveBoard(&myMatrix, 0, 0)
	}
}
~~~~

BenchmarkWithGorutines-4        100000000               12.9 ns/op

BenchmarkWithoutGorutines-4     300000000                4.59 ns/op

so they don't help.

final version

~~~~
package main

import (
	"fmt"
	"github.com/oelmekki/matrix"
	"strconv"
)

func printMatrix(matrix *matrix.Matrix) {

	for i := 0; i < matrix.Rows(); i++ {
		for j := 0; j < matrix.Cols(); j++ {
			fmt.Print(strconv.Itoa(int(matrix.At(i, j))) + " ")
		}
		fmt.Println()
	}

}

func solveBoard(matrix *matrix.Matrix, x int, y int) bool {

	if (x == 498) && (y == 498) {
		return true
	}

	if (x >= 0) && (y >= 0) && (x < 499) && (y < 499) && (matrix.At(x, y) == 0) {

		matrix.SetAt(x, y, -1)

		//going all directions
		if solveBoard(matrix, x+1, y) {
			return true
		}

		if solveBoard(matrix, x, y+1) {
			return true
		}

		if solveBoard(matrix, x-1, y) {
			return true
		}

		if solveBoard(matrix, x, y-1) {
			return true
		}

		matrix.SetAt(x, y, 2)
		return false
	}
	return false

}

var myMatrix = matrix.GenerateMatrix(499, 499)

func main() {

	for i := 0; i < myMatrix.Rows(); i++ {
		for j := 0; j < myMatrix.Cols(); j++ {
			if IsMine(i, j) {
				myMatrix.SetAt(i, j, 1)
			}

		}
	}

	solveBoard(&myMatrix, 0, 0)

	printMatrix(&myMatrix)

	//count how many 2s we have
	steps := 0
	for i := 0; i < myMatrix.Rows(); i++ {
		for j := 0; j < myMatrix.Cols(); j++ {
			if myMatrix.At(i, j) == 2 {
				steps += 1
			}
		}
	}

	fmt.Printf("\n Total steps: %d\n", steps)

}
~~~~~

