#### Pseudocode:

~~~~
visitedstates = set()
statestovisit = queue()
statestovisit.append(0,0)
visitedstates.append(0,0)

 while steastovisit not empty {

    states = statetovisit.pop()
    neighbours = getValidNeigbours(state)

    for n in neighbours {

        if not isMine(n) and n not in visitedstates{
                statestovisit.append(n)
                visitedstates.add(n)
        }
    }
 }

~~~~
