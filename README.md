# My final solution


### Version 1:
trying to use backtrack and displaying it on a matrix

useful: visually you can see that the max point would be  (0,499) so we would have a matrix of 0,499


### Version 2 (correct)

using a Set & a Queue

Since Go doesn't have this datastructures, we need to check how other people implemented them.

Pseudocode:

~~~~
visitedstates = set()
statestovisit = queue()
statestovisit.append(0,0)
visitedstates.append(0,0)

 while steastovisit not empty {

    states = statetovisit.pop()
    neighbours = getValidNeigbours(state)

    for n in neighbours {

        if not isMine(n) and n not in visitedstates{
                statestovisit.append(n)
                visitedstates.add(n)
        }
    }
 }

~~~~

####  Where getValidNeigbours is an array containing points -> up, down, left, right


### Conclusion:


##### what if instead of 21, the mine would be placed at > sum of coordinates > 211111 ?

the limit in golang of uint64 is 18446744073709551615. so it will overflow

One solution would be plotting the resulting number of steps on a graph and see how that graph looks like and if it looks easy to "draw".

This would require some geometry skills
