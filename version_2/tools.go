package main

import (
	"strings"
	"strconv"
)

// gets sum of the digits of the number (absolute value)
func getSumOfDigits(nr int) int {
	strs := strings.Split(strconv.Itoa(nr), "")

	// negative numbers are cut
	if strs[0] == "-" {
		strs = strs[1:]
	}

	// store them in an array
	ary := make([]int, len(strs))
	for i := range ary {
		ary[i], _ = strconv.Atoi(strs[i])
		ary[i] = int(ary[i])
	}

	// calculate the sum
	sum := 0
	for _, num := range ary {
		sum += num
	}
	return sum
}

// return true if it's a mine, false if it's not
func IsMine(x int, y int) bool {

	sumX := getSumOfDigits(x)
	sumY := getSumOfDigits(y)

	if sumX+sumY > 21 {
		return true
	}
	return false
}
