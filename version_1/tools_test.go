package version_1

import (
	"testing"
	"math"
)

func TestSumOfDigitsSimple(t *testing.T) {
	total := getSumOfDigits(112)
	if total != 4 {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", total, 4)
	}
}


func TestSumOfDigitsNegative(t *testing.T) {
	total := getSumOfDigits(-11111)
	if total != 5 {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", total, 5)
	}
}


// edge cases
func TestSumOfDigitEdge(t *testing.T) {
	total := getSumOfDigits(math.MaxInt64)
	if total != 88 {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", total, 88)
	}
}


func TestMineNegative(t *testing.T) {
	isMine := IsMine(1,1)
	if isMine {
		t.Errorf("Sould not be isMine = true here.")
	}
}

func TestMineNegative2(t *testing.T) {
	isMine := IsMine(-113,-104)
	if isMine {
		t.Errorf("Sould not be isMine = true here.")
	}
}

func TestMinePositive(t *testing.T) {
	isMine := IsMine(59,-79)
	if !isMine {
		t.Errorf("Sould not be isMine = false here.")
	}
}

func TestMinePositive2(t *testing.T) {
	isMine := IsMine(99,99)
	if !isMine {
		t.Errorf("Sould not be isMine = false here.")
	}
}